function openKCFinder(field) {
    var div = document.getElementById('kcfinder_div');
    if (div.style.display == "block") {
        div.style.display = 'none';
        div.innerHTML = '';
        return;
    }
    window.KCFinder = {
        callBack: function(url) {
            window.KCFinder = null;
            field.value = url;
            div.style.display = 'none';
            div.style.height = '150px';
            div.style.width = '150px';
            div.innerHTML = '';
        }
    };
 
    div.innerHTML = '<iframe name="kcfinder_iframe" src="/radiodm/admin/plugins/kcfinder/browse.php?type=files&dir=files/public" ' +
        'frameborder="1" width="100%" height="100%" marginwidth="0" marginheight="0" scrolling="no" />';
    div.style.display = 'block';
}

