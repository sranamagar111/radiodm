<?php
    if ($_COOKIE['firstVisit'] != "yes") {
      setcookie("firstVisit", "yes", time()+86400);
    }
    include_once("includes/application_top.php");


        /*Check if user have logged in or not*/
    if(!isset($_SESSION['username']) && !isset($_SESSION['password'])){
        header("location: login.php");
    }

        /*Including Header*/
    include 'partials/header.php';

        /*Including Top Nav Bar*/
    include 'partials/top_nav_bar.php';

        /*Including Main Nav Sidebar*/
    include 'partials/main_nav_sidebar.php';

        /*Including Main Content*/
    include 'app/masterPage.php';
    echo $pageContent;

        /*Including Footer*/
    include 'partials/footer.php';


?>
