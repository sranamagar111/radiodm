<?php
    class currentURL{
        public function currentUrl( $trim_query_string = false ) {
            $pageURL = (isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on') ? "https://" : "http://";
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            if( ! $trim_query_string ) {
                if (strpos($pageURL, '&') !== false) {
                    $pos = strpos($pageURL, '&');
                }
                elseif(strpos($pageURL,'?') !== false){
                    $pos=strpos($pageURL,'?');
                }
                else{
                    $pageURL = 'dashboard';
                    $pos = false;
                }
                $length = strlen($pageURL);
                if($pos!==false){
                    return substr($pageURL,$pos+1,$length);
                }
                else{
                    return $pageURL;
                }
            } else{
                $url = explode( '?', $pageURL );
                return $url[0];
            }
        }

        public function poss(){
            return pos;
        }
    }