<?php

    include_once("includes/application_top.php");
    if(isset($_SESSION['username']) && isset($_SESSION['password'])){
        header("location: index.php");
    }

        /*Including Header*/
    include 'partials/header.php';
?>

<body class="blank">

<!-- Wrapper-->
<div class="wrapper">


    <!-- Main content-->
    <section class="content">
        <div class="container-center animated slideInDown">
            <div class="view-header">
                <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-unlock"></i>
                </div>
                <div class="header-title">
                    <h3>Login</h3>
                    <small>
                        Please enter your credentials to login.
                    </small>
                </div>
            </div>

            <div class="panel panel-filled">
                <div class="panel-body">
                    <form action="process_login.php" id="loginForm" novalidate method="post">
                        <div class="form-group">
                            <!--Displaying the Invalid username-password Combination-->
                            <?php
                                if(isset($_SESSION['message'])){
                                    echo '<center><mark>'.$_SESSION['message'].'</mark></center>';
                                    unset($_SESSION['e']);
                                }
                            ?>
                            <label class="control-label" for="username" name="username">Username</label>
                            <input type="text" placeholder="username..." title="Please enter you username" required="" value="" name="username" id="username" class="form-control">
                            </div>
                        <div class="form-group">
                            <label class="control-label" for="password" name="password">Password</label>
                            <input type="password" title="Please enter your password" placeholder="*********" required="" value="" name="password" id="password" class="form-control">
                            </div>
                        <div>
                            <button class="btn btn-accent">Login</button>
                            <a class="btn btn-default" href="register.html">Register</a>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>
    <!-- End main content-->

<?php
        /*Including Header*/
    include 'partials/footer.php';
?>
