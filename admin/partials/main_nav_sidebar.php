<!-- Navigation-->
<aside class="navigation">
    <nav>
        <ul class="nav luna-nav">
            <li class="nav-category"></li>
            <li <?php
            if($currURL === 'dashboard'){
                echo 'class=active';
            }
            ?>">
            <a href="index.php">
                <i class="fa fa-home" aria-hidden="true"></i> Dashboard</a>
            </li>

            <li <?php
            if($currURL === 'module=news&action=list' || $currURL === 'module=news&action=newsForm' ||$currURL === 'module=news&subModule=newsCategory&action=list'){
                echo 'class=active';
            }
            ?>>
                <a href="#news" data-toggle="collapse" aria-expanded="false">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i> News<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="news" class="nav nav-second collapse">
                    <li <?php
                    if($currURL === 'module=news&action=list'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=news&action=list">All News</a>
                    </li>
                    <li <?php
                    if($currURL === 'module=news&action=newsForm'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=news&action=newsForm">Add New</a>
                    </li>
                    <li <?php
                    if($currURL === 'module=news&subModule=newsCategory&action=list'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=news&subModule=newsCategory&action=list">News Category</a>
                    </li>
                </ul>
            </li>
            <li <?php
            if($currURL === 'module=media&action=list' || $currURL === 'module=media&action=add'){
                echo 'class=active';
            }
            ?>>
                <a href="#media" data-toggle="collapse" aria-expanded="false">
                    <i class="fa fa-file-image-o" aria-hidden="true"></i> Media<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="media" class="nav nav-second collapse">
                    <li <?php
                    if($currURL === 'module=media&action=list'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=media&action=list">Library</a>
                    </li>
                    <li <?php
                    if($currURL === 'module=media&action=add'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=media&action=add">Add New</a>
                    </li>
                </ul>
            </li>
            <li <?php
            if($currURL === 'module=pages&action=list'){
                echo 'class=active';
            }
            ?>>
                <a href="#pages" data-toggle="collapse" aria-expanded="false">
                    <i class="fa fa-files-o" aria-hidden="true"></i></span> Pages<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="pages" class="nav nav-second collapse">
                    <li <?php
                    if($currURL === 'module=pages&action=list'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=pages&action=list">All Pages</a>
                    </li>
                </ul>
            </li>
            <li <?php
            if($currURL === 'module=banner&action=list' || $currURL === 'module=banner&action=bannerForm'){
                echo 'class=active ';
            }
            ?>>
                <a href="#banner" data-toggle="collapse" aria-expanded="false">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i> Banner<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="banner" class="nav nav-second collapse">
                    <li <?php
                    if($currURL === 'module=banner&action=list'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=banner&action=list">All Banner</a>
                    </li>
                    <li <?php
                    if($currURL === 'module=banner&action=bannerForm'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=banner&action=bannerForm">Add New</a>
                    </li>

                </ul>
            </li>
            <li <?php
            if($currURL === 'module=users&action=list' || $currURL === 'module=users&action=userForm' || $currURL === 'module=users&action=showProfile'){
                echo 'class=active';
            }
            ?>>
                <a href="#users" data-toggle="collapse" aria-expanded="false">
                    <i class="fa fa-user" aria-hidden="true"></i> Users<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="users" class="nav nav-second collapse">
                    <li <?php
                    if($currURL === 'module=users&action=list'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=users&action=list">All Users</a>
                    </li>
                    <li <?php
                    if($currURL === 'module=users&action=bannerForm'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=users&action=userForm">Add New</a>
                    </li>
                    <li <?php
                    if($currURL === 'module=users&action=showProfile'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=users&action=showProfile">Your Profile</a>
                    </li>
                </ul>
            </li>
            <li <?php
            if($currURL === 'module=settings&view=basic' || $currURL === 'module=settings&view=advance'){
                echo 'class=active';
            }
            ?>>
                <a href="#settings" data-toggle="collapse" aria-expanded="false">
                    <i class="fa fa-cog" aria-hidden="true"></i> Settings<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="settings" class="nav nav-second collapse">
                    <li <?php
                    if($currURL === 'module=settings&view=basic'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=settings&view=basic">General</a>
                    </li>
                    <li <?php
                    if($currURL === 'module=settings&view=basic'){
                        echo 'class=active';
                    }
                    ?>>
                        <a href="index.php?page=module&module=settings&view=advance">Advance</a>
                    </li>
                </ul>
            </li>

            <li class="nav-info">
                <i class="pe pe-7s-shield text-accent"></i>

                <div class="m-t-xs">
                    <span class="c-white">ABC</span> All the ABC content here.........
                </div>
            </li>
        </ul>
    </nav>
</aside>
<!-- End navigation-->