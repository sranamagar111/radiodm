</div>
<!-- End wrapper-->

<!-- Vendor scripts -->
<script src="vendor/pacejs/pace.min.js"></script>
<script src="vendor/jquery/dist/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/toastr/toastr.min.js"></script>
<script src="vendor/sparkline/index.js"></script>
<script src="vendor/flot/jquery.flot.min.js"></script>
<script src="vendor/flot/jquery.flot.resize.min.js"></script>
<script src="vendor/flot/jquery.flot.spline.js"></script>
<script src="vendor/datatables/datatables.min.js"></script>
<script src="vendor/nestable/jquery.nestable.js"></script>
<script src="vendor/kcfinder/kcfinder.js"></script>
<script src="scripts/toaster.js"></script>

<!--CKEditor Scripts-->
<script type="text/javascript" src="plugins/ckeditor/ckeditor.js"></script>

<!-- App scripts -->
<script src="scripts/luna.js"></script>

<?php
    if($_COOKIE['firstVisit'] != "yes"){
        ?>
        <script>
            var value = "<strong>Welcome !!!</strong> <br/><small>To Admin Panel. </small>";
            toaster('success',value);
        </script>
        <?php
    }
    if($currURL === 'module=pages&action=list') {
        echo '<script src="scripts/nestable.js"></script>';
    }
    ?>
    <script>
        $('.addNews').click(function () {
            for ( instance in CKEDITOR.instances )
                CKEDITOR.instances[instance].updateElement();
            var addData = $('#addUpdateNews').serializeArray();
            $.post('app/news/gui/CRUD.php',{"addData":addData},function(d){
                <?php
                    $funObj->flush_table();
                    $funObj->table = "news_details";
                    $sql ="SELECT ID FROM news_details ORDER BY ID DESC LIMIT 1";
                    $result = $funObj->exec($sql);
                    $id = $funObj->fetch_assoc($result);
                    $id = $id['ID'] + 1 ;
                    $url = "index.php?page=module&module=news&action=edit&id=".$id;
                    echo 'var url = "'.$url.'";';
                ?>
                window.location.href = url;
                var addMessage = "<strong>News</strong> <br/><small>Successfully Added !!! </small>";
                successToaster(addMessage);
            });
        });
        $('.updateNews').click(function () {
            if (CKEDITOR.instances['newsDescription']) {
                CKEDITOR.instances['newsDescription'].destroy();
            }
            CKEDITOR.replace('newsDescription');
            var updateData = $('#addUpdateNews').serializeArray();
            console.log("Update News");
            console.log(updateData);
            $.post('app/news/gui/CRUD.php',{"updateData":updateData},function(d){
                var updateMessage = "<strong>News</strong> <br/><small>Successfully Updated !!! </small>";
                successToaster(updateMessage);
            });
        });
        $('.saveDraft').click(function () {
            var data = $('#addUpdateNews').serializeArray();
            console.log("Save News as Draft");
            console.log(data);
            $.post('app/news/gui/CRUD.php',{"data":data},function(d){
                var draftMessage = "<strong>News</strong> <br/><small>News saved as Draft!!! </small>";
                warningToaster(draftMessage);
            });
        });
    </script>

<?php
    if($currURL === 'module=news&action=list' || $currURL === 'module=news&subModule=newsCategory&action=list' || $currURL === 'module=users&action=list' || $currURL === "module=banner&action=list"){
        echo '<script src="scripts/dataTable.js"></script>';
    }

    $funObj->close();
?>
</body>
</html>