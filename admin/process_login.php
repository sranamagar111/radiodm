<?php

    session_start();

    include_once("../includes/application_top.php");

    $funObj->tableField = "*";
    $funObj->table = "users";
    $funObj->condition = array();
    $funObj->order = "id DESC";
    $single = TRUE;

    $username = $_POST['username'];
    $password = md5($_POST['password']);


    $funObj->condition = array(
        "user_login" => $username,
        "user_pass" => $password
    );

    $result = $funObj->select();    


    $result = $funObj->exec($result);

    $admin = mysqli_fetch_array($result);

    $count = count($admin);
    if($count > 0){
        $_SESSION['username'] = trim(stripslashes($admin['user_login'])) ;
        $_SESSION['password'] = $admin['user_pass'] ;
        header("Location: index.php");
    }
    else{
        $_SESSION['message'] = "Invalid User/Pass";
        header("Location: login.php");
    }

?>