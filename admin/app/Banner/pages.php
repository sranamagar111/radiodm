<?php

    $action = $_GET['action'];
    if(isset($action)){
        switch($action){
            case "list":
                include'gui/bannerList.php';
                break;
            case "bannerForm":
                include'gui/addOrEditBanner.php';
                break;
            case "add":
                include'gui/CRUD.php';
                break;
            case "delete":
                include 'gui/CRUD.php';
                break;
            case "edit":
                include 'gui/addOrEditBanner.php';
                break;
            default:
                include('gui/newsList.php');
        }
    }

?>
