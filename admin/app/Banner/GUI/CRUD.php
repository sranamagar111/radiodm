<?php
/**
 * Created by PhpStorm.
 * User: Freeware Sys
 * Date: 1/8/2018
 * Time: 7:07 AM
 */

    $funObj->tableField = "*";
    $funObj->table = "banner";
    $funObj->condition = array();
    $funObj->order = "id DESC";

    $action = isset($_GET['action'])?$_GET['action']:"";
    $id = isset($_GET['id'])?$_GET['id']:"";

    $url = "index.php?page=module&module=banner&action=list";


if(isset($_POST)){
    if($action == 'add'){
        $bannerImage = end(explode("/",$_POST['bannerImage'])) ;
        $funObj->data = array(
            "banner_image" => $bannerImage,
            "banner_position" => $funObj->check($_POST['bannerPosition']),
            "permalink" => $funObj->check($_POST['bannerLink'])
        );
        $funObj->insert();
        $funObj->redirect($url);
    }
}

if($action == 'delete'){
    $funObj->condition = array("id"=>$id);
    $funObj->delete();
    $funObj->redirect($url);
}
