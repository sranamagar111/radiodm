<section class="content" xmlns:width="http://www.w3.org/1999/xhtml">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        Add Banner
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="index.php?page=module&module=banner&action=add" method="post">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="bannerLink">Link</label>
                                    <input type="text" class="form-control" name="bannerLink" id="bannerLink" placeholder="Banner Link ......" value="<?php if(isset($row['news_title'])) echo $row['news_title']; else echo ''; ?>" autofocus>
                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="bannerPosition">Banner Position</label>
                                            <select class="select2_demo_2 form-control" name="bannerPosition">
                                                <option>Select Banner Position</option>
                                                <option>First Adv. Position</option>
                                                <option>Second Adv. Position</option>
                                                <option>Third Adv. Position</option>
                                                <option>Before Manoranjan Sec. Left Adv. Position</option>
                                                <option>Before Manoranjan Sec. Right Adv. Position</option>
                                                <option>Before Khelkud Sec. Adv. Position</option>
                                                <option>After Khelkud Sec. Adv. Position</option>
                                                <option>Above Footer Section Position</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="bannerImage">Banner Image</label>
                                            <div id="kcfinder_div"></div>
                                            <input type="text" id="bannerImage" name="bannerImage" readonly="readonly" value="Click here to browse the server" onclick="openKCFinder(this)" style="width:100%;cursor:pointer" /><br />
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="panel-body">
                                            Advertisement Position Display
                                            <div id="vi">
                                                <img id="blah" src="#" alt="your image" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <input type="hidden" name="addHidden" value="1"/>
                                <input type="submit" class="btn btn-default pull-right" value="Add" name="add">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>