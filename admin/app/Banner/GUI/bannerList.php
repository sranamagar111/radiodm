<?php
    $funObj->tableField = "*";
    $funObj->table = "banner";
    $funObj->condition = array();
    $funObj->order = "id DESC";
    $single = FALSE;
    $result = $funObj->select();
?>
<!-- Main content-->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        List of Advertisement
                    </div>
                    <div class="panel-body">
                        <div class="m-t-md">
                            <a href="index.php?page=module&module=banner&action=bannerForm"">
                            <button type="button" class="btn btn-w-md btn-primary pull-right">
                                Add New
                            </button>
                            </a>
                            <br><br><br>

                        </div>
                        <div class="table-responsive">
                            <table id="tableExample2" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 5%">S.N.</th>
                                    <th style="width: 12%">Adv. Position</th>
                                    <th style="width: 35%">Advertisement Banner</th>
                                    <th style="width: 48%">Advertisement Link</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                if($result) {
                                    $res = $funObj->exec($result);
                                    $count = 1;
                                    while ($row = $funObj->fetch_assoc($res)) {
                                    ?>
                                        <tr id="parent">
                                            <td><?= $count ?></td>
                                            <td>
                                                <?= $row['banner_position'];?>
                                            </td>
                                            <td>
                                                <img src="/radiodm/admin/plugins/kcfinder/upload/files/<?= $row['banner_image'] ?>" id="bannerListImg">
                                                <br>
                                                <div id="hover-action-content">
                                                    <div class="row">
                                                        <div id="col-md-12">
                                                            <p>
                                                                <a href="index.php?page=module&module=banner&action=edit&id=<?= $row['id'] ?>">Edit</a>
                                                                |
                                                                <a href="index.php?page=module&module=banner&action=delete&id=<?= $row['id'] ?>" OnClick = "return confirm('Are You Sure?')">Delete</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><a href="//<?= $row['permalink']; ?>" target="_blank"><?= $row['permalink']; ?></a></td>
                                            <?php $count++; ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End main content-->