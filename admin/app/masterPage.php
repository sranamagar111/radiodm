<?php

    ob_start();

    if(!isset($_GET['page'])) {
        include "dashboard.php";
    }
    else{
        switch ($_GET['page']){
            case "module":
                $module = $_GET['module'];
                include('app/'.$module.'/pages.php');
                break;
            default:
                echo "page not found";
        }
    }

    $pageContent = ob_get_contents();
    ob_end_clean();

?>