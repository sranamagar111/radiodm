<!-- Main content-->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <i class="pe page-header-icon pe-7s-note"></i><strong> Profile</strong>
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="#">
                            <div>
                                <strong>Name: </strong>
                                <div class="form-group">
                                    <label for="username" class="col-sm-2 control-label">UserName: </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-group name="siteTitle" >
                                    </div>
                                    <label for="firstName" class="col-sm-2 control-label">First Name: </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-group name="firstName" >
                                    </div>
                                    <label for="lastName" class="col-sm-2 control-label">Last Name: </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-group name="lastName" >
                                    </div>
                                    <label for="nickname" class="col-sm-2 control-label">Nickname (<i>required</i>): </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-group name="adminEmailAddress" >
                                    </div>
                                    <label for="displayName" class="col-sm-2 control-label">Display name publicly as: </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-group name="displayName" >
                                    </div>
                                </div>
                                <div>
                                    <strong>Contact Info: </strong>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">Email (<i>required</i>): </label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control form-group name="email" >
                                        </div>
                                        <label for="website" class="col-sm-2 control-label">Website : </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control form-group name="website" >
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <strong>About Yourself: </strong>
                                    <div class="form-group">
                                        <label for="bio" class="col-sm-2 control-label">Biographical Info: </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control form-group name="bio" >
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <strong>Account Management: </strong>
                                    <div class="form-group">
                                        <label for="newPassword" class="col-sm-2 control-label">New Password: </label>
                                        <div class="col-sm-9">
                                            <button type="button" class="btn btn-default">Generate Password !!!</button>
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-default pull-right" value="Update Profile">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- End main content-->