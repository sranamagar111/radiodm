<?php
    /**
     * Created by PhpStorm.
     * User: Freeware Sys
     * Date: 1/8/2018
     * Time: 7:07 AM
     */

    $funObj->tableField = "*";
    $funObj->table = users;
    $funObj->condition = array();
    $funObj->order = "id DESC";

    $action = isset($_GET['action'])?$_GET['action']:"";
    $id = isset($_GET['id'])?$_GET['id']:"0";

    $url = "index.php?page=module&module=users&action=list";


    if(isset($_POST['add'])){
        $funObj->data = array(
            "category_name"=>$funObj->check($_POST['categoryName'])
        );

        $funObj->insert();
        $funObj->redirect($url);
    }

    if($action == 'delete'){
        $funObj->condition = array("id"=>$id);
        $funObj->delete();
        $funObj->redirect($url);
    }