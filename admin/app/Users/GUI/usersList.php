<?php
$funObj->tableField = "*";
$funObj->table = "users";
$funObj->cond = array();
$funObj->order = "id DESC";
$single = FALSE;
$result = $funObj->select();
?>
<!-- Main content-->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        List of all Users
                    </div>
                    <div class="panel-body">
                        <div class="m-t-md">
                            <a href="index.php?page=module&module=users&action=userForm"">
                            <button type="button" class="btn btn-w-md btn-primary pull-right">
                                Add New
                            </button>
                            </a>
                            <br><br><br>


                        </div>
                        <div class="table-responsive">
                            <table id="tableExample2" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Posts</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                if($result) {
                                    $res = $funObj->exec($result);
                                    $count = 1;
                                    while ($row = $funObj->fetch_assoc($res)) {
                                        ?>

                                        <tr id="parent">
                                            <td><?= $count ?></td>
                                            <td>
                                                <?= $row['user_login'] ?>
                                                <br>
                                                <div id="hover-content">
                                                    <a href="#" >Edit</a>
                                                    |
                                                    <a href="index.php?page=module&module=users&action=delete&id=<?= $row['ID'] ?>"
                                                       OnClick="return confirm('Are You Sure?')">Delete</a>
                                                </div>
                                            </td>
                                            <td><?= $row['display_name'] ?></td>
                                            <td><?= $row['user_email'] ?></td>
                                            <td>
                                                <?php
                                                    if($row['user_status'] == 0){
                                                        echo 'Administrator';
                                                    }
                                                    else{
                                                        echo 'Subscriber';
                                                    }
                                                ?>
                                            </td>
                                            <td><?= $row['user_status'] ?></td>
                                            <?php $count++; ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End main content-->