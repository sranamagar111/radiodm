<?php

    $action = $_GET['action'];
    if(isset($action)){
        switch($action){
            case "list":
                include'gui/usersList.php';
                break;
            case "userForm":
                include'gui/addOrEditUser.php';
                break;
            case "add":
                include'gui/CRUD.php';
                break;
            case "delete":
                include 'gui/CRUD.php';
                break;
            case "edit":
                include 'gui/addOrEditUser.php';
                break;
            case "showProfile":
                include 'gui/userProfile.php';
                break;
            default:
                include('gui/usersList.php');
        }
    }

?>
