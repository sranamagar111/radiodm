<!-- Main content-->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="view-header">
                    <div class="header-icon">
                        <i class="pe page-header-icon pe-7s-note"></i>
                    </div>
                    <div class="header-title">
                        <h3>General Settings</h3>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        ......
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="#">
                            <div class="form-group">
                                <label for="siteTitle" class="col-sm-2 control-label">Site Title: </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="siteTitle" autofocus required>
                                    <p></p>
                                </div>
                                <label for="siteTagline" class="col-sm-2 control-label">Tagline: </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="siteTagline" required>
                                    <p class="help-block">In a few words, explain what this site is about.</p>
                                </div>
                                <label for="siteFavicon" class="col-sm-2 control-label">Favicon: </label>
                                <div class="col-sm-9">
                                    <div id="image" onclick="openKCFinder(this)"><div style="margin:5px">Click here to choose an image</div></div>
                                    <div id="kcfinder_div" ></div>
                                    <input type="text" name="favicon" readonly="readonly" value="&emsp;Browse Image.." onclick="openKCFinder(this)" style="cursor:pointer" /><br />
                                    <p class="help-block">This is used for the favicon of the website.(Best-16*16 but 32*32 pixel can also be used)</p>
                                </div>
                                <label for="siteLogo" class="col-sm-2 control-label">Website Logo: </label>
                                <div class="col-sm-9">
                                    <div id="kcfinder_div" ></div>
                                    <div id="image" onclick="openKCFinder(this)"><div style="margin:5px">Click here to choose an image</div></div>
                                    <p class="help-block">This is used as the logo of the site.(1000*229 pixels)</p>
                                </div>
                                <label for="adminEmailAddress" class="col-sm-2 control-label">Email Address: </label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" name="adminEmailAddress" required>
                                    <p class="help-block">This address is used for admin purposes. If you change this we will send you an email at your new address to confirm it. The new address will not become active until confirmed.</p>
                                    <input type="submit" class="btn btn-default pull-right" value="Add">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- End main content-->