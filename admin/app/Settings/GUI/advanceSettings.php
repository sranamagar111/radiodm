<!-- Main content-->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="view-header">
                    <div class="header-icon">
                        <i class="pe page-header-icon pe-7s-note2"></i>
                    </div>
                    <div class="header-title">
                        <h3>Advance Settings</h3>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</section>
<!-- End main content-->