<!-- Main content-->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="view-header">
                    <div class="pull-right text-right" style="line-height: 14px">
                        <small>Luna Admin Theme<br>Dashboard<br> <span class="c-white">v.1.3</span></small>
                    </div>
                    <div class="header-icon">
                        <i class="pe page-header-icon pe-7s-shield"></i>
                    </div>
                    <div class="header-title">
                        <h3 class="m-b-xs">Luna Admin Theme</h3>
                        <?php
                            echo 'cms = '.CMS;
                            echo '<br>';
                        ?>
                        <small>
                            Special minimal admin theme with Dark UI style for monitoring or administration web applications.
                        </small>
                    </div>
                </div>
                <hr>
            </div>
        </div>

    </div>
</section>
<!-- End main content-->