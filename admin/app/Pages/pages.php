<?php

    $action = $_GET['action'];
    if(isset($action)){
        switch($action){
            case "list":
                include 'gui/pagesList.php';
                break;
            case "addForm":
                include 'gui/addOrEditPages.php';
                break;
            case "add":
                include 'gui/CRUD.php';
                break;
            case "delete":
                include 'gui/CRUD.php';
                break;
            case "edit":
                include 'gui/CRUD.php';
                break;
            case "sortMenu":
                if(isset($_POST['data'])){
                    $data = $_POST['data'];
                    echo $data;
                }
                break;
            default:
                include('gui/pagesList.php');
        }
    }

?>
