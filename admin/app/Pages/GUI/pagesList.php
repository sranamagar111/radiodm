<?php
    $funObj->tableField = "*";
    $funObj->table = "pages";
    $funObj->condition = array();
    $funObj->order = "id ASC";
    $single = FALSE;
?>

<!-- Main content-->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="view-header">
                    <div class="header-icon">
                        <i class="pe page-header-icon pe-7s-menu"></i>
                    </div>
                    <div class="header-title">
                        <h3>Pages list</h3>
                        <small>
                            Pages - Drag & drop hierarchical list.
                        </small>
                        <button class="saveMenu btn btn-w-md btn-primary pull-right">Save Menu</button>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="dd col-md-4 borderLine">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tabs-container">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Custom Link</a></li>
                                                <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false">Categories</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="tab-1" class="tab-pane active">
                                                    <div class="panel-body">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="panel-body">
                                                                    <label class="col-md-4 pull-left" for="URL">URL</label>
                                                                    <input id="menuURL" class="col-md-8 pull-right" type="text" value="http://">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="panel-body">
                                                                    <label class="col-md-4 pull-left" for="link">Link Text</label>
                                                                    <input id="menuLink" class="col-md-8 pull-right" type="text">
                                                                </div>
                                                            </div>
                                                            <button class="addToMenuCusLnk btn btn-w-md btn-warning pull-right">Add to Menu</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="tab-2" class="tab-pane">
                                                    <div class="panel-body">
                                                        <div class="panel-body">
                                                            <li><label> <input type="checkbox"> Check me out </label></li>
                                                            <li><label> <input type="checkbox"> Check me out </label></li>
                                                            <li><label> <input type="checkbox"> Check me out </label></li>
                                                        </div>
                                                        <button class="addToMenuCusLnk btn btn-w-md btn-warning pull-right">Add to Menu</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>           
                            </div>                                 
                        </div>
                        <div class="dd col-md-7 borderLine" id="nestable">
                            <div class="panel panel-c-accent">
                                <div class="panel-body">
                                    <h3>Menu Structure</h3>
                                    <p class="help-block">
                                        Drag each item into the order you prefer.
                                    </p>
                                    <?php
                                    $funObj->condition = array(
                                        "page_parent" => 0,
                                        "page_status" => 1
                                    );
                                    $result = $funObj->select();
                                    if($result){
                                        $result = $funObj->exec($result);
                                        if($funObj->total_rows($result) > 0){
                                            ?>
                                            <ol class="dd-list">
                                                <?php
                                                while ( $row = $funObj->fetch_assoc($result) ){
                                                    ?>
                                                    <li class="dd-item" data-id="<?= $row['ID'] ?>">
                                                        <div class="dd-handle">
                                                            <?= $row['page_title'] ?>
                                                        </div>
                                                        <?php
                                                        $funObj->condition = array(
                                                            "page_parent" => $row['ID'],
                                                            "page_status" => 1
                                                        );
                                                        $result1 = $funObj->select();
                                                        if($result1){
                                                            $result1 = $funObj->exec($result1);
                                                            if($funObj->total_rows($result1) > 0 ){
                                                                ?>
                                                                <ol class="dd-list">
                                                                    <?php
                                                                    while ( $row1 = $funObj->fetch_assoc($result1) ){
                                                                        ?>
                                                                        <li class="dd-item" data-id="<?=  $row1['ID'] ?>">
                                                                            <div class="dd-handle">
                                                                                <?= $row1['page_title'] ?>
                                                                            </div>
                                                                            <?php
                                                                            $funObj->condition = array(
                                                                                "page_parent" => $row1['ID'],
                                                                                "page_status" => 1
                                                                            );
                                                                            $result2 = $funObj->select();
                                                                            if($result2){
                                                                                $result2 = $funObj->exec($result2);
                                                                                if($funObj->total_rows($result2) > 0 ){
                                                                                    ?>
                                                                                    <ol class="dd-list">
                                                                                        <?php
                                                                                        while ( $row2 = $funObj->fetch_assoc($result2) ){
                                                                                            ?>
                                                                                            <li class="dd-item" data-id="<?= $row2['ID'] ?>" >
                                                                                                <div class="dd-handle">
                                                                                                    <?= $row2['page_title'] ?>
                                                                                                </div>
                                                                                                <?php
                                                                                                $funObj->condition = array(
                                                                                                    "page_parent" => $row2['ID'],
                                                                                                    "page_status" => 1
                                                                                                );
                                                                                                $result3 = $funObj->select();
                                                                                                if($result3){
                                                                                                    $result3 = $funObj->exec($result3);
                                                                                                    if($funObj->total_rows($result3) > 0 ){
                                                                                                        ?>
                                                                                                        <ol class="dd-list">
                                                                                                            <?php
                                                                                                            while ( $row3 = $funObj->fetch_assoc($result3) ){
                                                                                                                ?>
                                                                                                                <li class="dd-item" data-id="<?= $row3['ID'] ?>" >
                                                                                                                    <div class="dd-handle">
                                                                                                                        <?= $row3['page_title'] ?>
                                                                                                                    </div>
                                                                                                                    <?php
                                                                                                                    $funObj->condition = array(
                                                                                                                        "page_parent" => $row3['ID'],
                                                                                                                        "page_status" => 1
                                                                                                                    );
                                                                                                                    $result4 = $funObj->select();
                                                                                                                    if($result){
                                                                                                                        $result4 = $funObj->exec($result4);
                                                                                                                        if($funObj->total_rows($result4) > 0 ){
                                                                                                                            ?>
                                                                                                                            <ol class="dd-list">
                                                                                                                                <?php
                                                                                                                                while ( $row4 = $funObj->fetch_assoc($result4) ){
                                                                                                                                    ?>
                                                                                                                                    <li class="dd-item" data-id="<?= $row4['ID'] ?>" >
                                                                                                                                        <div class="dd-handle">
                                                                                                                                            <?= $row4['page_title'] ?>
                                                                                                                                        </div>
                                                                                                                                        <?php
                                                                                                                                        $funObj->condition = array(
                                                                                                                                            "page_parent" => $row4['ID'],
                                                                                                                                            "page_status" => 1
                                                                                                                                        );
                                                                                                                                        $result5 = $funObj->select();
                                                                                                                                        if($result5){
                                                                                                                                            $result5 = $funObj->exec($result5);
                                                                                                                                            if($funObj->total_rows($result5) > 0 ){
                                                                                                                                                ?>
                                                                                                                                                <ol class="dd-list">
                                                                                                                                                    <?php
                                                                                                                                                    while ( $row5 = $funObj->fetch_assoc($result5) ){
                                                                                                                                                        ?>
                                                                                                                                                        <li class="dd-item" data-id="<?= $row5['ID'] ?>" >
                                                                                                                                                            <div class="dd-handle">
                                                                                                                                                                <?= $row5['page_title'] ?>
                                                                                                                                                            </div>
                                                                                                                                                        </li>
                                                                                                                                                        <?php
                                                                                                                                                    }
                                                                                                                                                    ?>
                                                                                                                                                </ol>
                                                                                                                                                <?php
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                        ?>
                                                                                                                                    </li>
                                                                                                                                    <?php
                                                                                                                                }
                                                                                                                                ?>
                                                                                                                            </ol>
                                                                                                                            <?php
                                                                                                                        }
                                                                                                                    }
                                                                                                                    ?>
                                                                                                                </li>
                                                                                                                <?php
                                                                                                            }
                                                                                                            ?>
                                                                                                        </ol>
                                                                                                        <?php
                                                                                                    }
                                                                                                }
                                                                                                ?>
                                                                                            </li>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </ol>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </ol>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                            </ol>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="saveMenu btn btn-w-md btn-primary pull-right">Save Menu</button>
            </div>
        </div>
    </div>
</section>
<!-- End main content-->