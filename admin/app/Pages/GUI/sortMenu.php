<?php
    include("../../../../includes/Functions.php");

    error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    $funObj = new Functions;

    $con = $funObj->connect_db();

    $funObj->tableField = "*";
    $funObj->table = "pages";

    if(isset($_REQUEST['data'])){
        $data = $_REQUEST['data'];
        $pos = 1;
        foreach($data as $key => $value)
        {
            foreach($value as $k => $v) {
                $c1 = 1;
                if(!is_array($v)) {
                    $id = $value[$k];
                    $funObj->data = array(
                        "page_order" => $pos,
                        "page_parent" => 0
                    );
                    $funObj->condition = array(
                        "id" => $id
                    );
                    $funObj->update();
                    $pos++;
                    /*print_r($value[$k]);*/
                }
                else{
                    foreach($v as $a => $b) {
                        $c2 = 1;
                        $pos--;
                        foreach ($b as $c => $d){
                            if(!is_array($d)) {
                                $id2 = $b[$c];
                                $funObj->data = array(
                                    "page_order" => $c1,
                                    "page_parent" => $id
                                );
                                $funObj->condition = array(
                                    "id" => $id2
                                );
                                $funObj->update();
                                $c1++;
                                $pos++;
                                /*print_r($d);*/
                            }
                            else{
                                foreach($d as $e => $f) {
                                    $c1--;
                                    $c3 = 1;
                                    $pos--;
                                    foreach ($f as $g => $h) {
                                        if (!is_array($h)) {
                                            $id3 = $f[$g];
                                            $funObj->data = array(
                                                "page_order" => $c2,
                                                "page_parent" => $id2
                                            );
                                            $funObj->condition = array(
                                                "id" => $id3
                                            );
                                            $funObj->update();
                                            $c1++;
                                            $c2++;
                                            $pos++;
                                            /*print_r($h);*/
                                        }
                                        else{
                                            foreach($h as $i => $j) {
                                                $c1--;
                                                $c2--;
                                                $c4 = 1;
                                                $pos--;
                                                foreach ($j as $l => $m) {
                                                    if (!is_array($m)) {
                                                        $id4 = $j[$l];
                                                        $funObj->data = array(
                                                            "page_order" => $c3,
                                                            "page_parent" => $id3
                                                        );
                                                        $funObj->condition = array(
                                                            "id" => $id4
                                                        );
                                                        $funObj->update();
                                                        $c1++;
                                                        $c2++;
                                                        $c3++;
                                                        $pos++;
                                                        /*print_r($m);*/
                                                    }
                                                    else{
                                                        foreach($m as $n => $o) {
                                                            $c1--;
                                                            $c2--;
                                                            $c3--;
                                                            $pos--;
                                                            foreach ($o as $p => $q) {
                                                                if (!is_array($q)) {
                                                                    $id5 = $o[$p];
                                                                    $funObj->data = array(
                                                                        "page_order" => $c4,
                                                                        "page_parent" => $id4
                                                                    );
                                                                    $funObj->condition = array(
                                                                        "id" => $id5
                                                                    );
                                                                    $funObj->update();
                                                                    $c1++;
                                                                    $c2++;
                                                                    $c3++;
                                                                    $c4++;
                                                                    $pos++;
                                                                    /*print_r($q);*/
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
        echo 'Menu Successfully Updated !!!';
    }