<?php

    $action = $_GET['action'];
    if(isset($action)){
        switch($action){
            case "list":
                include 'gui/newsCategoryList.php';
                break;
            case "add":
                include 'gui/CRUD.php';
                break;
            case "delete":
                include 'gui/CRUD.php';
                break;
            case "edit":
                include 'gui/CRUD.php';
                break;
            default:
                include('gui/newsCategoryList.php');
        }
    }

?>
