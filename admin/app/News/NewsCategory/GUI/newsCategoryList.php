<?php
    $funObj->tableField = "*";
    $funObj->table = "categories";
    $funObj->condition = array();
    $funObj->order = "id ASC";
    $single = FALSE;
    $result = $funObj->select();
?>
<!-- Main content-->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        List of all News Category
                    </div>
                    <div class="panel-body">
                        <div class="m-t-md">

                            <button type="button" class="btn btn-w-md btn-primary pull-right" data-toggle="modal" data-target="#addNewsCategoryModal">
                                Add New
                            </button>
                            <br><br><br>

                                <!--Include the add/edit modal box-->
                            <?php include "addOrEditNewsCategories.php"; ?>

                        </div>
                        <div class="table-responsive">
                            <table id="tableExample2" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                if($result) {
                                    $res = $funObj->exec($result);
                                    $count = 1;
                                    while ($row = $funObj->fetch_assoc($res)) {
                                        ?>

                                        <tr id="parent">
                                            <td>
                                                <?= $count ?>
                                            </td>
                                            <td>
                                                <?= $row['category_name'] ?>
                                                <br>
                                                <div id="hover-content">
                                                    <a href="" data-toggle="modal" data-target="#editNewsCategoryModal" style="cursor: pointer;" >Edit</a>
                                                    |
                                                    <a href="index.php?page=module&module=news&subModule=newsCategory&action=delete&id=<?= $row['ID'] ?>"
                                                       OnClick="return confirm('Are You Sure?')">Delete</a>
                                                </div>
                                            </td>
                                            <?php $count++; ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End main content-->