<div class="modal fade" id="addNewsCategoryModal" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header text-center">
                <i class="fa fa-close pull-right" data-dismiss="modal" style="cursor: pointer;"></i>
                <h4 class="modal-title">Add new News Category</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="index.php?page=module&module=news&subModule=newsCategory&action=add" method="post">
                    <div class="form-group">
                        <label for="categoryName" class="col-sm-3 control-label">Category Name: </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-group" name="categoryName" autofocus required>
                        </div>
                        <input type="hidden" name="addHidden" value="1"/>
                        <input type="submit" class="btn btn-default pull-right" value="Add" name="add">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>