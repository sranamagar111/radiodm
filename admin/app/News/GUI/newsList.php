<?php
    $funObj->tableField = "*";
    $funObj->table = "news_details";
    $funObj->condition = array();
    $funObj->order = "id DESC";
    $single = FALSE;
    $result = $funObj->select();
?>
<!-- Main content-->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        List of all News
                    </div>
                    <div class="panel-body">
                        <div class="m-t-md">
                            <a href="index.php?page=module&module=news&action=newsForm"">
                                <button type="button" class="btn btn-w-md btn-primary pull-right">
                                    Add New
                                </button>
                            </a>
                            <br><br><br>


                        </div>
                        <div class="table-responsive">
                            <table id="tableExample2" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 5%">S.N.</th>
                                    <th style="width: 40%">Title</th>
                                    <th style="width: 15%">Author</th>
                                    <th style="width: 15%">Categories</th>
                                    <th style="width: 15%">Published</th>
                                    <th style="width: 10%">Status</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                if($result) {
                                    $res = $funObj->exec($result);
                                    $count = 1;
                                    while ($row = $funObj->fetch_assoc($res)) {
                                        $funObj->flush_table();
                                        $funObj->table = "categories";
                                        $funObj->condition =array(
                                            "ID" => $row['news_category']
                                        );
                                        $results = $funObj->select();
                                        if($results)
                                            $ressa = $funObj->exec($results);
                                        $rows = $funObj->fetch_assoc($ressa);
                                    ?>
                                        <tr id="parent">
                                            <td><?= $count ?></td>
                                            <td>
                                                <?= $row['news_title'] ?>
                                                <br>
                                                <div class="row">
                                                    <div id="col-md-12">
                                                        <div id="hover-action-content">
                                                            <p>
                                                                <a href="index.php?page=module&module=news&action=edit&id=<?= $row['ID'] ?>">Edit</a>
                                                                |
                                                                <a href="index.php?page=module&module=news&action=showNews&id=<?= $row['ID'] ?>">View</a>
                                                                |
                                                                <a href="index.php?page=module&module=news&action=delete&id=<?= $row['ID'] ?>" OnClick = "return confirm('Are You Sure?')">Delete</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="row">
                                                    <div id="col-md-12">
                                                        <div id="hover-content">
                                                            <?= substr($row['news_content'],0,300).'  .  .  .'  ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <?php
                                                if($row['news_author'] == 0){
                                                    echo 'Administrator';
                                                }
                                                else{
                                                    echo 'Subscriber';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?= $rows['category_name'];?>
                                            </td>
                                            <td><?= $row['news_date'] ?></td>
                                            <td><?= $row['news_status'] ?></td>
                                            <?php $count++; ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End main content-->