<?php
    $funObj->tableField = "*";
    $funObj->table = "news_details";
    $funObj->condition = array();
    $selected = 0;
    if($_GET['action'] === "edit"){
        $ID = $_GET['id'];
        $funObj->condition = array(
            "ID" => $ID
        );
        $result = $funObj->select();
        if($result) {
            $res = $funObj->exec($result);
            $row = $funObj->fetch_assoc($res);
            $selected = $row['news_category'];
        }
    }
?>
<section class="content" xmlns:width="http://www.w3.org/1999/xhtml">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-filled">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            <a class="panel-close"><i class="fa fa-times"></i></a>
                        </div>
                        <?php
                        if($_GET['action'] === "edit"){
                            ?>
                            Update News
                            <?php
                        }
                        else{
                            ?>
                            Add News
                        <?php } ?>
                    </div>
                    <div class="row">
                        <form action="" method="post" id="addUpdateNews">
                            <div class="col-md-8">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="newsTitle">News Title</label>
                                        <input type="text" class="form-control" name="newsTitle" id="newsTitle" placeholder="News Title ......" value="<?php if(isset($row['news_title'])) echo $row['news_title']; else echo ''; ?>" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="newsCategory">Category</label>
                                        <select class="select2_demo_2 form-control" name="newsCategory" style="width: 25% ">
                                            <option>Select News Category</option>
                                            <?php
                                                $funObj->flush_table();
                                                $funObj->table = "categories";
                                                $funObj->order = "id ASC";
                                                $results = $funObj->select();
                                                if($results) {
                                                $ress = $funObj->exec($results);
                                                $count = 1;
                                                while ($rows = $funObj->fetch_assoc($ress)) {
                                            ?>
                                            <option value="<?= $rows['ID'] ?>" <?php if($rows['ID'] == $selected) echo 'selected'; ?>><?= $rows['category_name'] ?></option>
                                            <?php } } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="newsDescription">News Description</label>
                                        <textarea class="form-control ckeditor" id="newsDescription" placeholder="News Description here ......" name="newsDescription" ><?php if(isset($row['news_content'])) echo $row['news_content']; else echo ''; ?></textarea>
                                    </div>    
                                </div>
                            </div>    
                            <div class="col-md-4">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="featuredImage">Featured Image</label>
                                        <div id="kcfinder_div"></div>
                                        <input type="text" id="bannerImage" name="bannerImage" readonly="readonly" value="Click here to browse the server" onclick="openKCFinder(this)" style="width:100%;cursor:pointer" /><br />
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label> <input type="checkbox" name="allowComments" value="1" <?php if(isset($row['comment_status'])) if($row['comment_status'] == "open") echo 'checked'; else echo ''; ?> > Allow Comments for this News</label>
                                    </div>
                                    <?php
                                        if($_GET['action'] === "edit"){
                                    ?>
                                            <input type="hidden" name="updateHidden" value="1"/>
                                            <input type="hidden" name="id" value="<?= $ID ?>"/>
                                            <button type="button" class="updateNews btn btn-success" name="update_btn">Update news</button>
                                    <?php
                                        }
                                        else{
                                    ?>
                                            <input type="hidden" name="addHidden" value="1"/>
                                            <button type="button" class="addNews btn btn-success" name="add_btn">Add News</button>
                                            <button type="button" class="saveDraft btn btn-warning" name="draft_btn">Save as Draft</button>
                                        <?php } ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>