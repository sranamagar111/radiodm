<?php
/**
 * Created by PhpStorm.
 * User: Freeware Sys
 * Date: 1/8/2018
 * Time: 7:07 AM
 */

$action = isset($_GET['action'])?$_GET['action']:"";

if($action != 'delete'){
    include("../../../includes/Functions.php");

    error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    $funObj = new Functions;
}

$con = $funObj->connect_db();
$funObj->flush_table();
$funObj->tableField = "*";
$funObj->table = "news_details";
$funObj->condition = array();
$funObj->order = "id DESC";

$id = isset($_GET['id'])?$_GET['id']:"0";
$url = "index.php?page=module&module=news&action=list";

if($action == 'delete'){
    $funObj->condition = array("id"=>$id);
    $funObj->delete();
    $funObj->redirect($url);
}

if(isset($_REQUEST['addData'])) {
    $data = $_REQUEST['addData'];
    foreach( $data as $key => $value ){
        if($value["name"] != 'addHidden') {
            if ($value["name"] == 'newsTitle') {
                $resultArray = array(
                    "news_title" => $value['value']
                );
                $funObj->data = array_merge($funObj->data ,$resultArray );
            }
            if ($value["name"] == 'newsCategory') {
                $resultArray = array(
                    "news_category" => $value['value']
                );
                $funObj->data = array_merge($funObj->data ,$resultArray );
            }
            if ($value["name"] == 'newsDescription') {
                $resultArray = array(
                    "news_content" => $value['value']
                );
                $funObj->data = array_merge($funObj->data ,$resultArray );
            }
            if ($value["name"] == 'allowComments') {
                $resultArray = array(
                    "comment_status" => 'open'
                );
                $funObj->data = array_merge($funObj->data ,$resultArray );
            }
        }
    }
    $funObj->insert();

}

if(isset($_REQUEST['updateData'])) {
    $data = $_REQUEST['updateData'];
    /*print_r($data);
    die();*/
    foreach( $data as $key => $value ){
        if($value["name"] != 'updateHidden') {
            if ($value["name"] == 'newsTitle') {
                $resultArray = array(
                    "news_title" => $value['value']
                );
                $funObj->data = array_merge($funObj->data ,$resultArray );
            }
            if ($value["name"] == 'newsCategory') {
                $value["name"] = '';
                $resultArray = array(
                    "news_category" => $value['value']
                );
                $funObj->data = array_merge($funObj->data ,$resultArray );
            }
            if ($value["name"] == 'newsDescription') {
                $resultArray = array(
                    "news_content" => $funObj->check($value['value'])
                );
                $funObj->data = array_merge($funObj->data ,$resultArray );
            }
            if ($value["name"] == 'allowComments') {
                $resultArray = array(
                    "comment_status" => 'open'
                );
                $funObj->data = array_merge($funObj->data , $resultArray );
                date_default_timezone_set('Asia/Kathmandu');
                $funObj->data = array_merge($funObj->data , array("news_modified" => date('Y-m-d H:i:s', time())));
            }
            if ($value["name"] == 'id') {
                $funObj->condition = array("id"=>$value['value']);
            }
        }
    }
    $funObj->update();
}

