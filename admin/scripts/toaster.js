function successToaster(value){
    toastr.options = {
        "debug": false,
        "newestOnTop": false,
        "positionClass": "toast-top-right",
        "closeButton": true,
        "progressBar": true,
        "showEasing": "swing",
        "timeOut": "4000"
    };
    toastr.success(value);
}
function infoToaster(value){
    toastr.options = {
        "debug": false,
        "newestOnTop": false,
        "positionClass": "toast-top-right",
        "closeButton": true,
        "progressBar": true,
        "showEasing": "swing",
        "timeOut": "4000"
    };
    toastr.info(value);
}
function warningToaster(value){
    toastr.options = {
        "debug": false,
        "newestOnTop": false,
        "positionClass": "toast-top-right",
        "closeButton": true,
        "progressBar": true,
        "showEasing": "swing",
        "timeOut": "4000"
    };
    toastr.warning(value);
}
function errorToaster(value){
    toastr.options = {
        "debug": false,
        "newestOnTop": false,
        "positionClass": "toast-top-right",
        "closeButton": true,
        "progressBar": true,
        "showEasing": "swing",
        "timeOut": "4000"
    };
    toastr.error(value);
}