<?php
    /**
     * Created by PhpStorm.
     * User: Freeware Sys
     * Date: 1/5/2018
     * Time: 1:20 PM
     */

    session_start();

    include_once("Functions.php");

    error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    $funObj = new Functions;

    $con = $funObj->connect_db();

    require_once 'class/currentURL.php';
    $curURL = new currentURL();
    $currURL=$curURL->currentURL();