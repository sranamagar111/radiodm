<?php
    /**
     * Created by PhpStorm.
     * User: Freeware Sys
     * Date: 1/5/2018
     * Time: 11:26 AM
     */

    include_once('constants.php');

    class Database{
        /**
         * @var string
         */
        protected $con;
        /**
         * @var string
         */
        public $table;
        /**
         * @var string
         */
        public $tableField;
        /**
         * @var array
         */
        public $data;
        /**
         * @var array
         */
        public $condition;
        /**
         * @var string
         */
        public $order;
        /**
         * @var string
         */
        public $limit;

        /**
         * Database constructor.
         */
        function __construct()
        {
            $this->con = "";
            $this->table = "";
            $this->data = [];
            $this->tableField = "*";
            $this->order = "";
            $this->condition = [];
            $this->limit = "";
        }


        /**
         * Connect to Database
         * @return mysqli|string
         */
        function connect_db(){
            $this->con = mysqli_connect(HOST,USER, PASS, DATABASENAME) or die( "Failed Connecting to Server".mysqli_error( $this->con ) );
            mysqli_set_charset($this->con , 'utf8');
            return $this->con;

        }

        /**
         * Execute query
         * @param $sql
         * @return bool|mysqli_result
         */
        function exec($sql){
            $result = mysqli_query( $this->con, $sql ) or die( "Wrong Query ".mysqli_error($this->con) );
            if($result){
                return $result;
            }
        }

        /**
         * fetch associative array type
         * @param $result
         * @return array|null
         */
        function fetch_assoc($result){
            return mysqli_fetch_assoc($result);
        }

        /**
         * Total rows in executed query
         * @param $result
         * @return int
         */
        function total_rows($result){
            return mysqli_num_rows($result);
        }

        /**
         * Select query Builder
         * @return string
         */
        function select(){
            $query = "SELECT $this->tableField FROM $this->table ";
            $carr = array();
            if( $this-> condition != "" )
            {
                foreach( $this->condition as $k => $v )
                {
                    $carr[$k] = "$k = '$v'";
                }
                if( count( $carr ) > 0 )
                {
                    $cstr = " WHERE ".implode(" AND ", $carr );
                    $query .= $cstr;
                }
            }

            if( $this->order != "" )
            {
                $query .= " ORDER BY ".$this->order." ";
            }

            if( $this->limit != "" )
            {
                $query .= " LIMIT ".$this->limit." ";
            }
            /*echo "<br>query = ".$query.'<br>';
            die();*/
            return $query;
        }

        /**
         * Insert new row
         * @return bool|mysqli_result
         */
        function insert(){
            $query = "INSERT INTO $this->table SET ";

            foreach ($this->data as $k => $v){
                $arr[$k] = " $k = '$v' ";
            }

            if(count($arr) > 0){
                $str = implode(",",$arr);
            }

            $query = $query.$str;

            echo "<br>query = ".$query.'<br>';
            $result = $this->exec($query);

            return $result;

        }

        /**
         * Delete row
         * @return bool|mysqli_result
         */
        function delete(){
            $query = "DELETE FROM $this->table ";

            foreach ($this->condition as $k => $v){
                $carr[$k] = " $k = '$v' ";

                if(count($carr) > 0){
                    $cstr = " WHERE ".implode(" AND ", $carr);
                    $query .= $cstr;
                }

                $result = $this->exec($query);
                return $result;
            }
        }

        /**
         * Update row
         * @return bool|mysqli_result
         */
        function update(){
            $query = "UPDATE $this->table SET ";

            foreach( $this->data as $k => $v )
            {
                $arr[$k] = "$k = '$v'";
            }
            if( count( $arr ) > 0 )
            {
                $str = implode(",", $arr);
                $query .= $str;
            }
            foreach( $this->condition as $k => $v )
            {
                $carr[$k] = "$k = '$v'";
            }
            if( count( $carr ) > 0 )
            {
                $cstr = " WHERE ".implode(" AND ", $carr );
                $query .= $cstr;
            }
            /*echo "<br>query = ".$query.'<br>';
            echo("<script>console.log('PHP: ".$query."');</script>");*/
            $result = $this->exec( $query );
            return $result;
        }

        /**
         * Closes association with the Database
         */
        public function close()
        {
            mysqli_close($this->con);
        }
    }