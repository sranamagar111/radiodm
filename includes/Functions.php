<?php
    /**
     * Created by PhpStorm.
     * User: Freeware Sys
     * Date: 1/5/2018
     * Time: 1:15 PM
     */

include_once("Database.php");
/**
     * Class Functions
     */
    class Functions extends Database{

        /**
         * Functions constructor.
         */
        public function __construct(){
           $this->flush_table();
       }

       public function flush_table(){
           $this->table = "";
           $this->data = [];
           $this->tableField = "*";
           $this->order = "";
           $this->condition = [];
           $this->limit = "";
       }

        /**
         * Check field
         * @param $field
         * @return string
         */
       function check($field){
            $field = strip_tags($field);
            $field = trim($field);
            $field = stripslashes($field);
            $field = mysqli_real_escape_string($this->con, $field);
            return $field;
       }

        /**
         * Redirect to provided url
         * @param $url
         */
       function redirect($url){
           if(headers_sent()){
               echo "<script type = \"text/javascript\">window.location.href = '$url';</script>";
               exit();
           }
           else{
               session_write_close();
               header("location: $url");
               exit();
           }
       }


    }