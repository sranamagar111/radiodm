<?php

    /*Include Application Top Section*/
    include 'includes/application_top.php';

    /*Include Header Section*/
    include 'partials/header.php';

    /*Include Top Logo Section*/
    include 'partials/topLogoSection.php';

    /*Include Top Navigation Section*/
    include 'partials/topNav.php';

    /*Include Main Section*/
    
    $funObj->tableField = "*";
    $funObj->table = "news_details";
    $funObj->condition = array();
    $funObj->order = "id DESC";
    $single = FALSE;
?>

<!-- content section -->
<div class="content">
    <div class="row">
        <!-- main content -->
        <div class="col-md-8 col-sm-8">
            <div class="main-content">            
                <div class="pg-section">
                    <!-- news slider -->
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox"></div>
                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                        </a>
                    </div>
                </div>
            
                <!-- ad -->
                <!-- taja samachar        -->
                <div class="news-block pg-section">
                    <div class="heading">
                        <a href="https://radiodailymail.com/news" rel="pages.php?pageName=news&category=26" class="link">View All<i class="fa fa-long-arrow-right"></i></a>
                        <h4 class="news-head">प्रदेश ७ समाचार</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <?php
                                $funObj->condition =  array(
                                    'news_category' => 1 ,
                                );
                                $funObj->limit = 1;
                                $result = $funObj->select();
                                if($result) {
                                    $row = $funObj->fetch_assoc($funObj->exec($result));
                                    $ida = $row['ID'];
                                }
                            ?>
                            <div class="news-detail">
                              <div class="news-img">
                                    <figure>
                                        <a href="news.php?id=<?= $row['ID']; ?>"><img src="/radiodm/admin/plugins/kcfinder/upload/files/<?= $row['featured_image'] ?>" alt="प्रदेश नम्बर सातमा पहिलोपटक प्रादेशिक सरकार गठन, यस्तो छ मन्त्रीको भागभण्डा"></a>
                                    </figure>
                                </div>

                                <h5 class="news-title"><a href="news.php?id=<?= $row['ID']; ?>"><?= $row['news_title']; ?></a></h5>
                                <p>
                                    <?php
                                        $s = substr($row['news_content'], 0, 300);
                                        echo substr($s, 0, strrpos($s, ' ')).' . . . ';
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="news-list">
                                <ul>
                                    <?php
                                        $funObj->condition =  array(
                                            'news_category' => 1 ,
                                        );
                                        $funObj->limit = 6;
                                        $result = $funObj->select();
                                        if($result) {
                                            $res = $funObj->exec($result);
                                            while ($row = $funObj->fetch_assoc($res)) {
                                    ?>
                                    <li>
                                        <a href="news.php?id=<?= $row['ID']; ?>"><?php echo $row['news_title']; ?></a>
                                    </li>
                                    <?php } }?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- national and international -->
                <div class="news-block pg-section">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="heading">
                            <a href="https://radiodailymail.com/news/category/37" rel="pages.php?pageName=news&category=37" class="link">View All<i class="fa fa-long-arrow-right"></i></a>
                            <h4 class="news-head">राष्ट्रिय</h4>
                        </div>

                        <div class="news-detail">
                            <?php
                                $funObj->condition =  array(
                                    'news_category' => 2 ,
                                );
                                $funObj->limit = 1;
                                $result = $funObj->select();
                                if($result) {
                                    $row = $funObj->fetch_assoc($funObj->exec($result));
                                    $ida = $row['ID'];
                                }
                            ?>
                            <div class="news-img">
                                <figure>
                                    <a href="news.php?id=<?= $row['ID']; ?>"><img src="/radiodm/admin/plugins/kcfinder/upload/files/<?= $row['featured_image'] ?>" alt="मुख्यमन्त्रीको मासिक तलब ६० हजार ९ सय ७०"></a>
                                </figure>
                            </div>

                            <h5 class="news-title"><a href="news.php?id=<?= $row['ID']; ?>"><?= $row['news_title']; ?></a></h5>
                            <p>
                                <?php
                                    $s = substr($row['news_content'], 0, 300);
                                    echo substr($s, 0, strrpos($s, ' ')).' . . . ';
                                ?>
                            </p>
                        </div>
                        <div class="news-list">
                            <ul>
                                <?php
                                    $funObj->condition =  array(
                                        'news_category' => 2 ,
                                    );
                                    $funObj->limit = 6;
                                    $result = $funObj->select();
                                    if($result) {
                                        $res = $funObj->exec($result);
                                        while ($row = $funObj->fetch_assoc($res)) {
                                ?>
                                <li>
                                    <a href="news.php?id=<?= $row['ID']; ?>"><?php echo $row['news_title']; ?></a>
                                </li>
                                <?php } }?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6">
                        <div class="heading">
                            <a href="https://radiodailymail.com/news/category/37" rel="pages.php?pageName=news&category=37" class="link">
                                View All
                                <i class="fa fa-long-arrow-right"></i>
                            </a>
                        <h4 class="news-head">अन्तराष्ट्रिय</h4>
                        </div>
                        <div class="news-detail">
                            <?php
                                $funObj->condition =  array(
                                    'news_category' => 3 ,
                                );
                                $funObj->limit = 1;
                                $result = $funObj->select();
                                if($result) {
                                    $row = $funObj->fetch_assoc($funObj->exec($result));
                                    $ida = $row['ID'];
                                }
                            ?>
                            <div class="news-img">
                                <figure>
                                    <a href="news.php?id=<?= $row['ID']; ?>"><img src="/radiodm/admin/plugins/kcfinder/upload/files/<?= $row['featured_image'] ?>" alt="मुख्यमन्त्रीको मासिक तलब ६० हजार ९ सय ७०"></a>
                                </figure>
                            </div>

                            <h5 class="news-title"><a href="news.php?id=<?= $row['ID']; ?>"><?= $row['news_title']; ?></a></h5>
                            <p>
                                <?php
                                    $s = substr($row['news_content'], 0, 300);
                                    echo substr($s, 0, strrpos($s, ' ')).' . . . ';
                                ?>
                            </p>
                        </div>
                        <div class="news-list">
                            <ul>
                                <?php
                                    $funObj->condition =  array(
                                        'news_category' => 3 ,
                                    );
                                    $funObj->limit = 6;
                                    $result = $funObj->select();
                                    if($result) {
                                        $res = $funObj->exec($result);
                                        while ($row = $funObj->fetch_assoc($res)) {
                                ?>
                                <li>
                                    <a href="news.php?id=<?= $row['ID']; ?>"><?php echo $row['news_title']; ?></a>
                                </li>
                                <?php } }?>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- below national and internal advertisement -->
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <figure>
                            <img src="images/belowNational/b9ccb8810200e003d94514e4c822808f.gif">
                        </figure><br>
                    </div>

                    <div class="col-md-6 col-sm-6">
                        <figure>
                            <img src="images/belowInternational/dc6b44f37c4c24ccff5ebe405d7478e2.gif">
                        </figure><br>
                    </div>
                </div>

                <!-- manoranjan -->
                <div class="blog pg-section">
                    <div class="heading">
                        <h4 class="news-head">मनोरन्जन </h4>
                        <a href="https://radiodailymail.com/news/category/28" rel="" class="link">
                            View All
                            <i class="fa fa-long-arrow-right"></i>
                        </a>
                    </div>
                    <?php
                        $funObj->condition =  array(
                            'news_category' => 3 ,
                        );
                        $funObj->limit = 6;
                        $result = $funObj->select();
                        if($result) {
                            $res = $funObj->exec($result);
                            while ($row = $funObj->fetch_assoc($res)) {
                                ?>
                                <div class="market-price">
                                    <div class="price-list">
                                        <div class="media">
                                            <a class="media-left" href="news.php?id=<?= $row['ID']; ?>">
                                                <img class="media-object" src="/radiodm/admin/plugins/kcfinder/upload/files/<?= $row['featured_image'] ?>" alt="" width="80px" height="80px">
                                                <div class="media-body blog-content">
                                                    <h4><?= $row['news_title']; ?></h4>
                                            </a>
                                                    <p>
                                                        <?php
                                                            $s = substr($row['news_content'], 0, 300);
                                                            echo substr($s, 0, strrpos($s, ' ')).' . . . ';
                                                        ?>
                                                    </p>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                    ?>
                </div>

                <!-- below manoranjan advertisement -->
                <div class="col-md-12 col-sm-12">
                    <figure>
                        <img src="images/belowEntertainment/bc7c3df346ec47760646f8706f5eb36e.gif">
                    </figure><br>
                </div>

                <!-- sports -->
                <div class="news-block pg-section">
                    <div class="heading">
                        <a href="https://radiodailymail.com/news/category/25" rel="pages.php?pageName=news&category=25" class="link">
                            View All
                            <i class="fa fa-long-arrow-right"></i>
                        </a>
                        <h4 class="news-head">खेलकुद</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <?php
                                $funObj->condition =  array(
                                    'news_category' => 7 ,
                                );
                                $funObj->limit = 1;
                                $result = $funObj->select();
                                if($result) {
                                    $row = $funObj->fetch_assoc($funObj->exec($result));
                                    $ida = $row['ID'];
                                }
                            ?>
                            <div class="news-detail">
                              <div class="news-img">
                                    <figure>
                                        <a href="news.php?id=<?= $row['ID']; ?>"><img src="/radiodm/admin/plugins/kcfinder/upload/files/<?= $row['featured_image'] ?>" alt="प्रदेश नम्बर सातमा पहिलोपटक प्रादेशिक सरकार गठन, यस्तो छ मन्त्रीको भागभण्डा"></a>
                                    </figure>
                                </div>

                                <h5 class="news-title"><a href="news.php?id=<?= $row['ID']; ?>"><?= $row['news_title']; ?></a></h5>
                                <p>
                                    <?php
                                        $s = substr($row['news_content'], 0, 300);
                                        echo substr($s, 0, strrpos($s, ' ')).' . . . ';
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="news-list">
                                <ul>
                                    <?php
                                        $funObj->condition =  array(
                                            'news_category' => 7 ,
                                        );
                                        $funObj->limit = 6;
                                        $result = $funObj->select();
                                        if($result) {
                                            $res = $funObj->exec($result);
                                            while ($row = $funObj->fetch_assoc($res)) {
                                    ?>
                                    <li>
                                        <a href="news.php?id=<?= $row['ID']; ?>"><?php echo $row['news_title']; ?></a>
                                    </li>
                                    <?php } }?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- below khelkud advertisement -->
                <div class="col-md-12 col-sm-12">
                    <figure>
                        <img src="images/belowSports/3361c04d93e7a81288beed517194bd7b.gif">
                    </figure><br>
                </div>

                <!-- Politics and Economics -->
                <div class="news-block pg-section">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="heading">
                                <a href="https://radiodailymail.com/news/category/37" rel="pages.php?pageName=news&category=37" class="link">
                                    View All
                                    <i class="fa fa-long-arrow-right"></i>
                                </a>
                                <h4 class="news-head">राजनीति</h4>
                            </div>
                            <div class="news-detail">
                                <?php
                                    $funObj->condition =  array(
                                        'news_category' => 4 ,
                                    );
                                    $funObj->limit = 1;
                                    $result = $funObj->select();
                                    if($result) {
                                        $row = $funObj->fetch_assoc($funObj->exec($result));
                                        $ida = $row['ID'];
                                    }
                                ?>
                                <div class="news-img">
                                    <figure>
                                        <a href="news.php?id=<?= $row['ID']; ?>"><img src="/radiodm/admin/plugins/kcfinder/upload/files/<?= $row['featured_image'] ?>" alt="मुख्यमन्त्रीको मासिक तलब ६० हजार ९ सय ७०"></a>
                                    </figure>
                                </div>

                                <h5 class="news-title"><a href="news.php?id=<?= $row['ID']; ?>"><?= $row['news_title']; ?></a></h5>
                                <p>
                                    <?php
                                        $s = substr($row['news_content'], 0, 300);
                                        echo substr($s, 0, strrpos($s, ' ')).' . . . ';
                                    ?>
                                </p>
                            </div>
                            <div class="news-list">
                                <ul>
                                    <?php
                                        $funObj->condition =  array(
                                            'news_category' => 4 ,
                                        );
                                        $funObj->limit = 6;
                                        $result = $funObj->select();
                                        if($result) {
                                            $res = $funObj->exec($result);
                                            while ($row = $funObj->fetch_assoc($res)) {
                                    ?>
                                    <li>
                                        <a href="news.php?id=<?= $row['ID']; ?>"><?php echo $row['news_title']; ?></a>
                                    </li>
                                    <?php } }?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="heading">
                                <a href="https://radiodailymail.com/news/category/37" rel="pages.php?pageName=news&category=37" class="link">View All<i class="fa fa-long-arrow-right"></i></a>
                                <h4 class="news-head">अर्थ</h4>
                            </div>
                            <div class="news-detail">
                                <?php
                                    $funObj->condition =  array(
                                        'news_category' => 6 ,
                                    );
                                    $funObj->limit = 1;
                                    $result = $funObj->select();
                                    if($result) {
                                        $row = $funObj->fetch_assoc($funObj->exec($result));
                                        $ida = $row['ID'];
                                    }
                                ?>
                                <div class="news-img">
                                    <figure>
                                        <a href="news.php?id=<?= $row['ID']; ?>"><img src="/radiodm/admin/plugins/kcfinder/upload/files/<?= $row['featured_image'] ?>" alt="मुख्यमन्त्रीको मासिक तलब ६० हजार ९ सय ७०"></a>
                                    </figure>
                                </div>

                                <h5 class="news-title"><a href="news.php?id=<?= $row['ID']; ?>"><?= $row['news_title']; ?></a></h5>
                                <p>
                                    <?php
                                        $s = substr($row['news_content'], 0, 300);
                                        echo substr($s, 0, strrpos($s, ' ')).' . . . ';
                                    ?>
                                </p>
                            </div>
                            <div class="news-list">
                                <ul>
                                    <?php
                                        $funObj->condition =  array(
                                            'news_category' => 2 ,
                                        );
                                        $funObj->limit = 6;
                                        $result = $funObj->select();
                                        if($result) {
                                            $res = $funObj->exec($result);
                                            while ($row = $funObj->fetch_assoc($res)) {
                                    ?>
                                    <li>
                                        <a href="news.php?id=<?= $row['ID']; ?>"><?php echo $row['news_title']; ?></a>
                                    </li>
                                    <?php } }?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12">
                    <figure>
                        <img src="images/mainBanner/c2d9c6e36db273a1c69e1b6516e0469f.gif">
                    </figure><br>
                </div>
                </div>
        </div>
    </div>
        
    <!-- side content -->
    <div class="col-md-4 col-sm-4">
        <div class="side-content">
            <!-- onair program -->
            <!-- advertisement -->
            <div class="col-md-13">
                <!--Advertisement Section-->
                <div class="ad-section pg-section" >
                    <div class="heading">
                        <h4 class="news-head">Advertisement</h4>
                    </div>
                    <figure>
                        <img src="images/topBanner/0a68ed4a14d5686c28707513df9a7c2d.gif">
                    </figure><br>

                    <figure>
                        <img src="images/topBanner/53729f9640e9bce429f35fa95179d968.jpg">
                    </figure><br>

                    <figure>
                        <img src="images/topBanner/18aa428203c35fcbf597675be7e90e92.gif">
                    </figure><br>
                </div>
            </div>

            <div class="poll">
                <h4 class="news-head">Poll</h4>
                <p class="news-title">सरकारले संविधान शंशोधन प्रस्ताव सदनमा लानुलाई के भन्नुहुन्छ ?</p>
                <div class="newpoll">
                    <form method="get" id="poll_form" action="#">
                        <div class="radio" id="">
                            <label>
                                <input type="radio" name="answer" value="13" checked>
                                प्रस्तावको औचित्य प्रष्ट्याउनुपर्छ
                            </label>
                        </div>
                        <div class="radio" id="">
                            <label>
                                <input type="radio" name="answer" value="14" >
                                आवश्यक थियो
                            </label>
                        </div>
                        <div class="radio" id="">
                            <label>
                                <input type="radio" name="answer" value="15" >
                                राष्ट्रघाती छ
                            </label>
                        </div>
                        <div class="radio" id="">
                            <label>
                                <input type="radio" name="answer" value="16" >
                                काम कुरो एकातिर कुम्लो बोकि अन्तैतिर
                            </label>
                        </div>
                        <input type="hidden" name="pollid" value="5"?>
                    </form>
                    <button class="btn btn-log" id="poll">Submit</button>
                    <button class="btn btn-log btn-red" id="pollresult">View Result</button>
                </div>
            </div><br><br>

            <!-- youtube video -->
            <div class="youtube pg-section">
                <div class="heading">
                    <h4 class="news-head">Video</h4>
                </div>
                     <figure>
                       <iframe width="337" height="227" src="https://www.youtube.com/embed/HzIHdoBjujE" frameborder="0" allowfullscreen></iframe>
                </figure>
            </div>
        </div>
    </div>
</div>

<?php
    /*Include Footer Section*/
    include 'partials/footer.php'; 

?>            
            