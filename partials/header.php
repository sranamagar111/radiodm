<!DOCTYPE html>
<html lang="en">

	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="जस्तो समाज , उस्तै आवाज । सामुदायिक रेडियो डेलीमेल" /> 
	<meta name="keywords" content="Dailymail FM, online radio, media house, radio broadcast, radio Daily Mail, listen radio" />
	<meta property="fb:app_id" content="1174311112650689" />
	<meta property="og:title" content="Radio Dailymail" />
	<meta property="og:site_name " content="Radio Melamchi " />
	<meta property="og:description" content="जस्तो समाज , उस्तै आवाज । सामुदायिक रेडियो डेलीमेल" />
	<meta property="og:image" content="images/default.jpg" /> 

	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="@RadioMelamchi">
	<meta name="twitter:title" content="Radio Dailymail">
	<meta name="twitter:description" content="जस्तो समाज , उस्तै आवाज । सामुदायिक रेडियो डेलीमेल">
	<meta name="twitter:image" content="images/default.jpg">  
	<link rel="icon" type="image/png" href="images/style/favicon.png" /> 


	<title>Radio Dailymail</title>
	<!-- Bootstrap -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<link href="css/font-awesome.css" rel="stylesheet">
	<link href="css/sm-blue.css" rel="stylesheet">
	<link href="css/newsticker.css" rel="stylesheet">
	<link href="css/jplayer.blue.monday.css" rel="stylesheet">

	<script type="text/javascript" src="../w.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">
	    stLight.options({publisher: "53dd24b7-cc9e-43ed-96ea-d65e37f561f4", doNotHash: false, doNotCopy: false, hashAddressBar: false});
	</script>
	<style>
	span.stMainServices.st-facebook-counter {
	    height: 22px;
	}

	span.stButton_gradient.stHBubble {
	    height: 22px;
	}

	span.stMainServices.st-twitter-counter {
	    height: 22px;
	}

	.share.no-flt {
	    padding: 10px 0;
	}
	</style>

	<!-- Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>

	<!-- Owl Carousel Assets -->
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/owl.theme.css" rel="stylesheet">

	<!-- fancy box -->
	<link rel="stylesheet" href="css/jquery.fancybox.css" media="screen">


	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>