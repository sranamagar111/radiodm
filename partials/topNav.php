<?php
    $funObj->tableField = "*";
    $funObj->table = "pages";
    $funObj->condition = array();
    $funObj->order = "id ASC";
    $single = FALSE;

?>
<div class="top-head top-nav">
    <aside  class="navbar" role="navigation">
        <!-- nav bar collapse -->
        <article class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </article>

        <article class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">    
            <ul id="main-menu" class="sm sm-blue">
                <li class="selected">
                    <a href="index.php">गृहपृष्‍ठ</a>
                </li>

                <?php
                    $funObj->condition = array(
                        "page_parent" => 0,
                        "page_status" => 1
                    );
                    $result = $funObj->select();
                    if($result){
                        $result = $funObj->exec($result);
                        if($funObj->total_rows($result) > 0){
                            while ( $row = $funObj->fetch_assoc($result) ){
                                ?>
                                <li class="">
                                    <a href="category.php?id=<?= $row['ID']; ?>"><?= $row['page_title'] ?></a>
                                    <?php
                                        $funObj->condition = array(
                                            "page_parent" => $row['ID'],
                                            "page_status" => 1
                                        );
                                        $result1 = $funObj->select();
                                        if($result1){
                                            $result1 = $funObj->exec($result1);
                                            if($funObj->total_rows($result1) > 0 ){
                                                ?>
                                                <ul>
                                                    <?php
                                                        while ( $row1 = $funObj->fetch_assoc($result1) ){
                                                            ?>
                                                            <li>
                                                                <a href="category.php?id=<?= $row1['ID']; ?>"><?= $row1['page_title'] ?></a>
                                                                <?php
                                                                    $funObj->condition = array(
                                                                        "page_parent" => $row1['ID'],
                                                                        "page_status" => 1
                                                                    );
                                                                    $result2 = $funObj->select();
                                                                    if($result2){
                                                                        $result2 = $funObj->exec($result2);
                                                                        if($funObj->total_rows($result2) > 0 ){
                                                                            ?>
                                                                            <ul>
                                                                                <?php
                                                                                    while ( $row2 = $funObj->fetch_assoc($result2) ){
                                                                                        ?>
                                                                                        <li>
                                                                                            <a href="category.php?id=<?= $row2['ID']; ?>"><?= $row2['page_title'] ?></a>
                                                                                        </li>
                                                                                        <?php
                                                                                    }
                                                                                ?>
                                                                            </ul>
                                                                            <?php
                                                                        }
                                                                    }
                                                                ?>                                                                    
                                                            </li>
                                                            <?php                                                                
                                                        }
                                                    ?>
                                                </ul>
                                                <?php
                                            }
                                        }
                                    ?>                               
                                </li>
                                <?php
                            }
                        }
                    }
                ?>                            
            </ul>
        </article>
    </aside>
</div> <!-- Top Nav -->