</div>
<footer>
    <div class="footer-top">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="footer-nav">
                    <h4 class="news-head">Quick links</h4>
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="https://radiodailymail.com/allnews">News</a></li>
                        <li><a href="https://radiodailymail.com/audio">Programs</a></li>
                        <li><a href="https://radiodailymail.com/gallery">Gallery</a></li>
                        <li><a href="https://radiodailymail.com/station-info">Station Info</a></li>
                        <li><a href="https://radiodailymail.com/management">Team</a></li>
                        <li><a href="https://radiodailymail.com/contact-us">Contact</a></li>
                    </ul>
                </div>
            </div>
            
            <!-- <div class="col-md-3 col-sm-6">
                <h4 class="news-head">Our Broadcast Partners</h4>
                
                <ul>
                    <li>ACORAB (CIN)</li>
                    <li>DDC Sinchupalchwok</li>
                    <li>HELVITAS</li>
                    <li>KARTAS Swiss</li>
                </ul>
            </div> -->
            
            <div class="col-md-4 col-sm-6">
                <h4 class="news-head">Contact Us</h4>
                <div class="box">
                    <p>Radio Daily Mail 94.6 Mhz</p>

                    <p>DailyMail Media Pvt. Ltd.</p>

                    <p>Mainroad, Dhangadhi-4, Kailali</p>

                    <p>Phone:091-525553<br />
                    Email: radio.dailymail@gmail.com</p>
                </div>
            </div>
            <!-- <div class="col-md-4 col-sm-6"> -->
            <!-- <h4 class="news-head" >media</h4><br> -->
            <!-- <a href="https://www.facebook.com/radiodailymail/" target="_new"><img src="css/site_css/images/facebook_new.png"></a><br><br>
            <a href="https://www.facebook.com/radiodailymail/" target="_new"><img src="css/site_css/images/twitter_new.png"></a> -->

            <div class="col-md-4 col-sm-12" ><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fdailymailfm%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></div>
                        
            <!-- </div> -->
                        
        </div>
    </div>
                
    <div class="footer-bottom">
        <p class="pull-right">Site by : <a href="http://www.softnep.com/" target="_new">SoftNEP</a></p>
        <p>© Copyright 2018 Radio Dailymail. All Right Reserved.</p>
    </div>
</footer>
<style type="text/css">
    .flash_player {
        display: none;
    }
</style>          
<script type="text/javascript" src="../code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".listen").click(function() {
            var id=$(this).attr('id');
            //alert("ID:"+id);  
            $('.btn'+ id).slideToggle("slow");
        });
    });
</script>        
</div>
</div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.11.2.js"></script>
<!-- Include all compiled plugins (below), or in>clude individual files as needed -->
<script src="js/bootstrap.js"></script>

<!-- nav-menu script -->
<script type="text/javascript" src="js/jquery.smartmenus.js"></script>
<script type="text/javascript">
  $(function() {
    $('#main-menu').smartmenus({
      subMenusSubOffsetX: 1,
      subMenusSubOffsetY: -20
    });
  });
</script>

<!-- owl carousel -->
<script src="js/owl.carousel.js"></script>
<script>
    $(document).ready(function() {

        $("#owl-demo").owlCarousel({
            items : 1,
            itemsDesktop : [1199,1],
            itemsDesktopSmall : [980,1],
            itemsTablet: [768,1],
            itemsTabletSmall: false,
            itemsMobile : [479,1],
            singleItem : false,
            speed : 2000,
            lazyLoad : true,
            navigation : true,
            navigationText : ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            pagination : false,
        });

        $("#owl-demo1").owlCarousel({
            items : 1,
            itemsDesktop : [1199,1],
            itemsDesktopSmall : [980,1],
            itemsTablet: [768,1],
            itemsTabletSmall: false,
            itemsMobile : [479,1],
            singleItem : false,
            speed : 2000,
            lazyLoad : true,
            navigation : true,
            navigationText : ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            pagination : false,
        });

        setInterval("displaytime()", 1000);
    });

    function displaytime(){
        var currentTime = new Date ( );    
        var currentHours = currentTime.getHours ( );   
        var currentMinutes = currentTime.getMinutes ( );   
        var currentSeconds = currentTime.getSeconds ( );
        currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;   
        currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;    
        var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";    
        currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;    
        currentHours = ( currentHours == 0 ) ? 12 : currentHours;    
        var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;
        document.getElementById("time").innerHTML = currentTimeString;  
    }
</script>

<!-- list scroller script -->
<script src="js/jquery.bootstrap.newsbox.js"></script>
<script type="text/javascript">
    $(function () {
        $(".demo3").bootstrapNews({  
         newsPerPage: 3,   
            autoplay: true,  
                onToDo: function () {  
                }
            });
    });
</script>

<script src="js/basics.js" type="text/javascript"></script>

<!-- Fancy box script -->
<script type="text/javascript" src="js/jquery.fancybox.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.fancybox').fancybox();
  });
</script>

<!-- live player script -->
<script type="text/javascript" src="js/jplayer.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
      $("#jquery_jplayer_01").jPlayer({
        ready: function (event) {
          $(this).jPlayer("setMedia", {
            mp3:"http://streaming.softnep.net:8055/;stream.nsv&type=mp3&volume=60"
          }).jPlayer("play");
        },
        cssSelectorAncestor:'#jp_container_01',
        swfPath: "system/js",
        supplied: "mp3",
        wmode: "window",
        volume: 0.75
        
      });
    });

    $('a.jp-play').trigger('click');
</script>
    
<div id="fb-root"></div>
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "../connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=1174311112650689";
        fjs.parentNode.insertBefore(js, fjs);
    }
    (document, 'script', 'facebook-jssdk'));
</script>

<?php $funObj-> close(); ?>

</body>
</html>