<?php

    /*Include Application Top Section*/
    include 'includes/application_top.php';

    /*Include Header Section*/
    include 'partials/header.php';

    /*Include Top Logo Section*/
    include 'partials/topLogoSection.php';

    /*Include Top Navigation Section*/
    include 'partials/topNav.php';

    /*Include Main Section*/
    $funObj->tableField = "*";
    $funObj->condition = array();
    $funObj->order = "id DESC";
    $single = FALSE;
    $id = $_GET['id'];
    $funObj->condition = array(
        'news_category' => $id, 
    );
?>

    <!-- content section -->
    <div class="content">
        <div class="row">
            <!-- main content -->
            <div class="col-md-8 col-sm-8">
                <div class="main-content">
                    <div class="heading">
                        <h1 class="news-head">
                            <?php
                                $funObj->table = "categories";
                                $funObj->condition = array(
                                    'ID' => $id, 
                                );
                                $result = $funObj->select();
                                if($result) {
                                    $row = $funObj->fetch_assoc($funObj->exec($result));
                                }
                            ?>
                            <?= $row['category_name']; ?>
                        </h1>
                     </div>
                   
                    <?php
                        $funObj->table = "news_details";
                        $funObj->condition = array(
                                    'news_category' => $id, 
                                );
                        $result = $funObj->select();
                        if($result) {
                            $res = $funObj->exec($result);
                            while ($row = $funObj->fetch_assoc($res)) {
                    ?>
                    <!-- news-list -->
                    <div class="media">
                        <div class="media-left">
                            <a href="category.php?id=<?= $row['ID']; ?>" >
                                <img class="media-object" src="/radiodm/admin/plugins/kcfinder/upload/files/<?= $row['featured_image'] ?>" alt="" width="180px">
                            </a>
                        </div>
                        
                        <div class="media-body">
                            <div class="text news">
                                <h4 class="news-head">
                                    <a href="news.php?id=<?= $row['ID']; ?>" >
                                        <?= $row['news_title']; ?>   
                                    </a>
                                </h4>
                                <p id="news_details">
                                    <?php 
                                        $s = substr($row['news_content'], 0, 300);
                                        echo substr($s, 0, strrpos($s, ' ')).' . . . ';
                                    ?>
                                </p>
                                <a class="link" href="news.php?id=<?= $row['ID']; ?>">Read More<i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <?php } } ?>                                                
                </div>
            </div>
            <!-- side content -->
            <div class="col-md-4 col-sm-4">
                <div class="side-content">
                    <!-- onair program -->
                    <!-- advertisement -->
                <div class="col-md-13">
                    <!-- notice board -->
                    <div class="notice pg-section">
                        <div class="panel panel-default">
                            <div class="row">
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fdailymailfm%2F&tabs&width=340&height=154&small_header=true&adapt_container_width=false&hide_cover=false&show_facepile=true&appId" width="100%" height="154" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                            </div>
                        </div>
                    </div>

                    <!-- youtube video -->
                    <div class="youtube pg-section">
                        <div class="heading">
                            <h4 class="news-head">Video</h4>
                        </div>
                        <figure>
                            <iframe width="100%" height="227" src="https://www.youtube.com/embed/HzIHdoBjujE" frameborder="0" allowfullscreen></iframe>
                        </figure>
                    </div>
                    <!--Advertisement Section-->
                    <div class="ad-section pg-section" >
                    <div class="heading">
                        <h4 class="news-head">Advertisement</h4>
                    </div>

                    <figure><img src="images/topBanner/0a68ed4a14d5686c28707513df9a7c2d.gif"></figure><br>
                    <figure><img src="images/topBanner/53729f9640e9bce429f35fa95179d968.jpg"></figure><br>
                    <figure><img src="images/topBanner/18aa428203c35fcbf597675be7e90e92.gif"></figure><br>

                    </div>
                    </div>
                    <div class="ad-section" >
                       <!--  <a href="https://play.google.com/store/apps/details?id=com.softnep.radiomelamchi" target="_blank"><figure><img src="images/app-load.png"></figure></a> -->
                    </div>

                    <!-- advertisement -->
                </div>
            </div>
        </div>
    </div>

<?php
    /*Include Footer Section*/
    include 'partials/footer.php'; 

?>            
            